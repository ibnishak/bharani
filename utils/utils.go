package utils

import (
	"bufio"
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.com/ibnishak/bharani/utils/readability"
	"log"
	//nurl "net/url"
	"os"
	//"time"
	"github.com/lunny/html2md"
	_ "github.com/mattn/go-sqlite3"
	"io/ioutil"
)

func Saveurl(s *string, d *string) {

	url := *s
	db := *d

	// Fetch readable content
	article, err := readability.NewReadability(url)
	if err != nil {
		panic(err)
	}
	article.Parse()
	mapD := map[string]string{"bag": "bag", "type": "text/vnd.tiddlywiki", "title": article.Title, "author": article.Title, "source": url}
	mapB, _ := json.Marshal(mapD)

	database, _ := sql.Open("sqlite3", db)
	statement, _ := database.Prepare(`
		CREATE TABLE IF NOT EXISTS wiki (id integer not null primary key AUTOINCREMENT, title text NOT NULL UNIQUE, meta text, content BLOB, revision integer, source text, author text);
	`)
	statement.Exec()
	statement, _ = database.Prepare(`
		CREATE TABLE IF NOT EXISTS wiki_history (id integer not null primary key AUTOINCREMENT, title text NOT NULL, meta text, content BLOB, revision integer, source text, author text);
	`)
	statement.Exec()
	statement, _ = database.Prepare("INSERT INTO wiki(title, meta, content, revision, source, author) VALUES (?, ?, ?, ?, ?, ?)")
	statement.Exec(article.Title, string(mapB), article.Content, 1, url, article.Author)
	fmt.Printf("\n\nAdded %s to %s\n", url, db)

	md := html2md.Convert(article.Content)
	d1 := []byte(md)
	err = ioutil.WriteFile("/home/richie/article1.txt", d1, 0644)
	if err != nil {
		fmt.Println("File write error. Error is", err)
	}
}

func Compactdb(d *string) {
	db := *d
	database, err := sql.Open("sqlite3", db)
	if err != nil {
		log.Fatal(err)
	}
	defer database.Close()
	statement, _ := database.Prepare("DELETE FROM wiki WHERE id  NOT in (SELECT max(id) from wiki GROUP BY title)")
	statement.Exec()
}

func Extractnotes(d *string, n *string) {
	db := *d
	notebook := *n
	database, err := sql.Open("sqlite3", db)
	if err != nil {
		log.Fatal(err)
	}
	defer database.Close()
	rows, err := database.Query("SELECT content FROM wiki WHERE title LIKE '%BharaniHighlight%'")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		err = rows.Scan(&name)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(name)
		f, err := os.OpenFile(notebook, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
		check(err)
		defer f.Close()
		w := bufio.NewWriter(f)
		fmt.Fprintf(w, "* %s\n", name)
		w.Flush()
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	notedelete, _ := database.Prepare("DELETE FROM wiki WHERE title LIKE '%BharaniHighlight%'")
	notedelete.Exec()
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
