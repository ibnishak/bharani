

<p align="center" style="text-align: center;">
<img src="https://i.imgur.com/CzzeCFO.png">
</p>

Bharani is an alternative to pocket that lets user to highlight, add notes and tag articles. It is build on [widdly by opennota](https://gitlab.com/opennota/widdly) with various modifications merged from fork by [cs8425](https://github.com/cs8425/widdly) and [xarnze](https://github.com/xarnze/widdly). It uses go readability project to convert article to readability format and adds it to an sqlite database. The front end is based on [TW5](http://tiddlywiki.com/).


## Installation
```
go get -u gitlab.com/ibnishak/bharani
go install $GOPATH/src/gitlab.com/ibnishak/bharani
cp $GOPATH/src/gitlab.com/ibnishak/bharani/index.html $GOPATH/bin
```

## Commandline flags
```
  -add
    	Url to be added to database
  -compact
    	Compact the database, delete the revisions.
  -db
    	Path to database file (defaults to "articles.db")
  -extract
    	Extract the notes to a text file (defaults to "inbox.org")
  -inbox
    	File to extract the highlights to (default "inbox.org")
  -port
    	HTTP service address (defaults to ":8060")
  -serve
    	Start the server

```

## Usage

- Adding an article
```
bharani -db myarticles.db -add https://medium.com/s/story/did-the-apple-watch-series-4-steal-the-show-3912ea0dd0f9 
```

- Viewing saved articles
```
bharani -serve -port :8076 -db myarticles.db

// Now open http://127.0.0.1:8076 in your browser
```


- View help

```
bharani -help

```

- Extracting the highlighted portions to a text file.
 - Remember - only the highlights made since last extraction will be written to the text file. It means that once extracted highlights cannot be extracted for a second time. It can still be viewed when the database is served.

```
bharani -extract hello.txt
```

