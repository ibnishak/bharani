package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/securecookie"
	"github.com/kardianos/osext"
	"gitlab.com/ibnishak/bharani/api"
	"gitlab.com/ibnishak/bharani/store"
	_ "gitlab.com/ibnishak/bharani/store/sqlite"
	"gitlab.com/ibnishak/bharani/utils"
)

var (
	serve      = flag.Bool("serve", false, "Start the server")
	addr       = flag.String("port", "127.0.0.1:8060", "HTTP service address")
	dataSource = flag.String("db", "articles.db", "Path to database file")
	addurl     = flag.String("add", "", "Url to be added to database")
	compact    = flag.Bool("compact", false, "Compact the database")
	extract    = flag.Bool("extract", false, "Extract the notes")
	inbox      = flag.String("inbox", "inbox.org", "File to extract the highlights to")

	hashKey      = securecookie.GenerateRandomKey(64)
	secureCookie = securecookie.New(hashKey, nil)
)

func main() {
	flag.Parse()
	if *serve == true {
		fmt.Println("[server] Open browser and navigate to ", *addr)
		api.Store = store.MustOpen(*dataSource)

		wiki := pathToWiki()
		api.ServeIndex = func(w http.ResponseWriter, r *http.Request) {
			if fi, err := os.Stat(wiki); err == nil && isRegular(fi) { // Prefer the real file, if it exists.
				http.ServeFile(w, r, wiki)
			} else {
				http.NotFound(w, r)
			}
		}
		log.Fatal(http.ListenAndServe(*addr, nil))
	} else if *addurl != "" {
		utils.Saveurl(addurl, dataSource)
	} else if *compact == true {
		utils.Compactdb(dataSource)
	} else if *extract == true {
		utils.Extractnotes(dataSource, inbox)
	}
}

func pathToWiki() string {
	dir := ""
	path, err := osext.Executable() //TODO(opennota): switch to the new os.Executable() once Go 1.8 is out.
	if err == nil {
		dir = filepath.Dir(path)
	} else if wd, err := os.Getwd(); err == nil {
		dir = wd
	}
	return filepath.Join(dir, "index.html")
}

// isRegular returns true iff the file described by fi is a regular file.
func isRegular(fi os.FileInfo) bool {
	return fi.Mode()&os.ModeType == 0
}
